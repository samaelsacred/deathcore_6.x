## ![logo](http://i.imgur.com/QBNaZYj.png) 


# DeathCore 6.x.x - New Source - Scripts


##Project General Information 

Patch 6.x - NDP-DB 6.x (Alpha)  

- Level 100;
- Daily corrections;
- Great stability;
- All spells are being worked;
- Professionally developed;
- All class and races combinations are fixed.

This is the new source of DeathCore Team that we are updating slowly. Sincerely, Team Death Noffearr ProjecT™.

##Required Files 

[DeathCore_6.x.x_Dbc](https://mega.nz/#!60MRSZCY!QbcMvu74rbGFUGvfD3cke64iSETB9WlcT9sOjaaRBEY)

[DeathCore_6.x.x_Maps](https://mega.nz/#!GoFEyZKR!SebeOfF8dRFZzgjSF0h5gAp-OjhA54uY-1tyPVSNcSM)

[DeathCore_6.x.x_Mmaps](https://mega.nz/#!XkV0zDhI!UjenfmnkG5Br99G1d2CuSlSV-ow95vSv4PCDwaYijS8)

[DeathCore_6.x.x_Vmaps](https://mega.nz/#!28lg1TRT!L8-Sp506HLoOxt9fuHf_v0QlQt0APZ9s1q0HRbTuOJw)


=====================

## ![logo](http://i.imgur.com/Ues1gtC.png)


## Introduction

Noffearr Death ProjecT™ it is a team of developers that creates MMORPG games mainly based on C ++. To acquire or have access to our projects, you'll have to pay them. Our sources are inspired on TrinityCore, Mangos and ScriptDev2. We do over time changes to optimize, improve and clean the base of the codes while, at the same time we improve the game mechanics and the game functionality.

This is a paid Source, however, the development of it depends from everyone. I'm hopping that the community that has access to this project has enough encouragement and will help us, even if it is by reporting bugs in our forum at our VIP Area.

For more information about the project, visit our website at project [Noffearr Death ProjecT™](http://noffearrdeathproject.net)

## Requirements

+ Platform: Linux, Windows or Mac
+ Processor with SSE2 support
+ Boost ≥ 1.49
+ MySQL ≥ 5.1.0
+ CMake ≥ 2.8.11.2 / 2.8.9 (Windows / Linux)
+ OpenSSL ≥ 1.0.0
+ GCC ≥ 4.7.2 (Linux only)
+ MS Visual Studio ≥ 12 (2013 Update 4) (Windows only)

## Install

Detailed installation guides are available in the [TrinityCore Wiki](http://collab.kpsn.org/display/tc/Installation+Guide) for
Windows, Linux and Mac OSX.


## Copyright

License: GPL 2.0

Read file [COPYING](COPYING)


## Authors & Contributors

Read file [THANKS](THANKS)


[Forums](http://www.noffearrdeathproject.net)